#include <iostream>
#include <string>

using namespace std;

string padZeroes(string input, int numsize) {
  while (input.size() < numsize) {
    input += "0";
  }
  return input;
}
