#include <iostream>
#include <fstream>
#include <string>
#include "shrinkName.cpp"
#include "padding.cpp"
#include "removeCollision.cpp"
using namespace std;
int read_file( char filename[], string names[]) {
    ifstream readfile;
    readfile.open(filename);
    int i = 0;
    while (!readfile.eof()) {
        string name;
        getline(readfile,  name);
        names[i] = name;
        i++;
    }
    readfile.close();
    return i - 1;
}
string map_char_to_num(char ch) {
    string alphabets[26] = {"2","2","2","3","3","3","4","4","4","5","5","5","6","6","6","7","7","7","7","8","8","8","9","9","9","9"};
    if (ch >= 'a' && ch <= 'z')
        return alphabets[ch - 'a'];
}

string convert_to_num(string name) {
    string num;
    for (int i = 0;i < name.length(); i++) 
        num += map_char_to_num(name.at(i));
    return num;
}
string map_name_to_number(string name, int map_of_number[], int num_size) {
  string ignored_str = "aeiou";
  string converted_name = shrinkName(ignored_str, name, num_size);
  string num = convert_to_num(converted_name);
  num = padZeroes(num, num_size);
  num = removeCollision(map_of_number, 100000, num);
  map_of_number[atoi(num.c_str())] = 1;
  return num;
}
void convert_names_to_numbers(string names[], string numbers[], int total_names) {
    int map_of_number[100000] = {0};
    int MAP_NUM_SIZE = 5;
    for (int i = 0;i < total_names; i++) {
        numbers[i] = map_name_to_number(names[i], map_of_number, MAP_NUM_SIZE); 
        cout << endl << names[i] << "\t" << numbers[i];
    }
}
int main (int argc, char* argv[])  {
    int MAX_SIZE = 100;
    if (argc == 2) {
        string names[MAX_SIZE];
        string numbers[MAX_SIZE];
        int total_names = read_file(argv[1], names);
        for (int i = 0;i < total_names; i++)
            cout<<endl<<names[i];
        convert_names_to_numbers(names, numbers, total_names);
   }
    else 
        cout <<"not got";
}
