#include <iostream>
#include <string>
#include <locale>
using namespace std;

string shrinkName(string ignoredLetters, string input, int size_output) {
  locale loc;
  ignoredLetters += " ";
  for (int i = 0; i < input.size(); ++i)
    {
      input[i] = tolower(input[i], loc);
    }
  for (int i = 0; i < ignoredLetters.size(); ++i)
    {
      input.erase (std::remove(input.begin(), input.end(), ignoredLetters[i]), input.end());
    }
  if (input.size() <= 5) {
    return input;
  }
  else {
    string output = input.substr(0, size_output);
    return output;
  }
}
//
//int main() {
  //string in;
  //getline(cin, in);
  //cout << shrinkName("aeiou",in,5) <<endl;
//}
