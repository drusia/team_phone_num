#include <iostream>
#include <string>
#include <sstream>
using namespace std;

string removeCollision(int map[], int size, string input) {
  int number = atoi(input.c_str());
  int i = number;
  while (map[i]) {
    i++;
  }
  stringstream output;
  output << i;
  return output.str();
}
